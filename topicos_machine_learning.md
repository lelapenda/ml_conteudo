#Projeto Delphos-Fenaseg 

#1. Métodos/Modelos

##Desafios 

Entre os desafios encontrados no problema de detecção de fraudes pode-se citar como principais:
* Desbalanceamento de classes
* Variação da distribuição com o tempo

Além destes, há desafios mais comuns como o problema dos *missing values* e atributos categóricos (quando o modelo suporta apenas atributos numéricos). Outro problema que pode ocorrer é chamado de *class overlap*.

###Desbalanceamente de classes
* Algorithmic Methods:
	* classificadores construídos especificamente para lidar com problemas de desbalanceamento
	* classificadores que minimizam a função de custo global
* Data Level Methods:
	* Sampling
	* Distance-based
	
#### Algorithmic Methods

#####Algoritmos mais afetados

*Decision Tree* é o mais afetado pelo desbalanceamento de classes. *MLP* é menos afetado que as árvores de decisão mas acredita-se que o *SVM* ainda seja o vencendor quando se trata de classes desbalanceadas. Em nível algorítimico é possível atribuir custos para predições de classe erradas com o objetivo de ajustar o algoritmo e gerar um modelo mais preciso.


#### Data Level Methods

#####Sampling Methods

* Undersampling
* Oversampling
* SMOTE

#####Distance-based

* Condensed Nearest Neighbor (CNN)


##Feature Selection

A escolha dos atributos que serão utilizados para treinar o modelo é fundamental para o sucesso da predição. A escolha pode ser feita por:

* Forwarding Selection
* Backwarding Elimination
* Dimensionality Reduction (PCA)

##O Modelo

###Algoritmos comumente utilizados

* Logistic Regression
* Modified Multi-variate Gaussian (MVG)
* Bagging using Random Forest


##Métricas 

Recall on invés de acurácia. Usar curva ROC.



#2. Links

1. COMPARATIVE ANALYSIS OF MACHINE LEARNING TECHNIQUES FOR DETECTING INSURANCE CLAIMS FRAUD -> https://www.wipro.com/documents/comparative-analysis-of-machine-learning-techniques-for-detecting-insurance-claims-fraud.pdf

2. Adaptive Machine Learning for Credit Card Fraud Detection -> http://www.ulb.ac.be/di/map/adalpozz/pdf/Dalpozzolo2015PhD.pdf